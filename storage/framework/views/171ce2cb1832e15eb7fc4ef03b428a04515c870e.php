<?php
$controller = app('App\Modules\Base\Http\Controllers\Controller');
$controller->css[] = 'login.min.css';
$controller->librerias = [
	'jquery-ui',
	'jquery-ui-timepicker',
	'bootstrap-sweetalert',
	'bootstrap-wizard',
	'alphanum', 
	'maskedinput', 
];
$controller->js= [
	'login.js',
	'moment.min.js',
	'jquery.validate.min.js',
	'additional-methods.min.js',
	'bootstrap-wizard/jquery.bootstrap.wizard.min.js',
	'select2.full.min.js'
];

$data = $controller->_app();
extract($data);

$html['titulo'] = 'Inicio de Sesión';
?>
<!DOCTYPE html>
<!--[if IE 8]>    <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>    <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--><html lang="es"><!--<![endif]-->
<head>
	<?php echo $__env->make('base::partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head><!--/head-->

	<body class="login">
		<div class="logo">
			
		</div>
		<div class="content" id="login">
			<?php echo Form::open(array('id' => 'formulario', 'url' => 'login')); ?>

				<h3 class="form-title font-green">Inicio de Sesión</h3>
				<div>
					<img alt="" class="profile-img"  id="foto" src="<?php echo e(url('public/img/usuarios/user.png')); ?>">
				</div>
				<div class="form-group">
					<label class="control-label visible-ie8 visible-ie9"><?php echo e(Lang::get('login.user')); ?></label>
					<?php echo Form::text('nombre', '', ['class' => 'form-control form-control-solid placeholder-no-fix user', 'autocomplete' => 'off', 'placeholder' => Lang::get('login.user')]); ?>

				</div>

				<div class="form-group">
					<label class="control-label visible-ie8 visible-ie9"><?php echo e(Lang::get('login.password')); ?></label>
					<?php echo Form::password('password', ['class' => 'form-control form-control-solid placeholder-no-fix', 'autocomplete' => 'off', 'placeholder' => Lang::get('login.password')]); ?>

				</div>

				<label class="rememberme check mt-checkbox mt-checkbox-outline">
					<?php echo Form::checkbox('recordar', '1', false); ?>

					<?php echo e(Lang::get('login.remember_me')); ?>

					<span></span>
				</label>
					<!-- 	Desarrollado por:   
							Alejandro Mendez alejmendez.87@gmail.com
							Omar monasterio omarmonasterio@gmail.com
							Miguelangel Gutierrez Drummermiguelangel@gmail.com 	
					 -->
				
				<a href="javascript:;" id="forget-password" class="forget-password">¿Recuperar Contraseña?</a>
					<div class="form-actions" style="text-align: center;">
						<?php echo Form::button(Lang::get('login.log_in'), ['class' => 'btn green uppercase']); ?>

					</div>
					<h6 class="form-title font-red">Nota: Sólo podrá Iniciar Sesión una vez creada la cuenta.</h6>

					<div class="create-account">
						<p>
							<a href="javascript:;"  id="register-btn" class="uppercase">Crea Una Cuenta Gratis</a>
						</p>
						
					</div>
				
			<?php echo Form::close(); ?>

		</div>
		<div id="registro" style="display: none;">
			<?php echo Form::open(['id' => 'submit_form', 'name' => 'formulario100', 'method' => 'POST' ]); ?>

			
				<div class="col-md-12">
					<div class="portlet light" id="form_wizard_1">
						<div class="portlet-title">
							<div class="caption">
								<i class=" icon-layers font-red"></i>
								<span class="caption-subject font-red bold uppercase">Registro de Nuevo Usuario
									<span class="step-title"> 1 de 4 </span>
								</span>
							</div>
							<div class="close" >
								<a href="javascript:;" class="" id="cerrar" style="font-size: 37px; color:#ff0a1a;">
									<i class="fa fa-times-circle-o" aria-hidden="true"></i>
								</a>	
							</div>
						</div>
						<div class="portlet-body form"> 
							<div class="form-wizard">
								<div class="form-body">
									<ul class="nav nav-pills nav-justified steps">
										<li>
											<a href="#tab1" data-toggle="tab" class="step">
												<span class="number"> 1 </span>
												<span class="desc">
													<i class="fa fa-check"></i>Datos Generales</span>
											</a>
										</li>
										<li>
											<a href="#tab2" data-toggle="tab" class="step">
												<span class="number"> 2 </span>
												<span class="desc">
													<i class="fa fa-check"></i>Recidencia</span>
											</a>
										</li>
										<li>
											<a href="#tab3" data-toggle="tab" class="step active">
												<span class="number"> 3 </span>
												<span class="desc">
													<i class="fa fa-check"></i>contacto</span>
											</a>
										</li>
										
										<li>
											<a href="#tab4" data-toggle="tab" class="step active">
												<span class="number"> 4 </span>
												<span class="desc">
													<i class="fa fa-check"></i> Seguridad </span>
											</a>
										</li>
										
									</ul>
									<div id="bar" class="progress progress-striped" role="progressbar">
										<div class="progress-bar progress-bar-success"> </div>
									</div>
									<div class="tab-content">
										<div class="alert alert-danger display-none">
											<button class="close" data-dismiss="alert"></button> Usted tiene algunos errores en el formulario. Por favor, compruebe.
										</div>
										<div class="alert alert-success display-none">
											<button class="close" data-dismiss="alert"></button> ¡La validación de su formulario es exitosa!
										</div>
										
										<div class="tab-pane active" id="tab1">
											<div class="panel ">
												
												<div class="panel-body">
													
												<div class="form-group col-md-4 cont-persona">
												<label for="nombres">Rif / Numero Cedula:</label>
												<div class="form-group multiple-form-group input-group">
												
													<div class="input-group-btn input-group-select">
														<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="concept">-</span><span class="caret"></span></button>
														<ul class="dropdown-menu" role="menu" required >
															<?php $__currentLoopData = $tipo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $tipo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<li><a href="<?php echo e($id); ?>" id="btn-<?php echo e($id); ?>"><?php echo e($tipo); ?></a></li>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</ul>
														<!-- RegistroRequest  -->
														<input id="tipo_persona" name="tipo_persona" class="input-group-select-val" type="hidden" />
													</div> 

														<?php echo e(Form::text('dni', '', [
															'id'          => 'dni',
															'class'       => 'form-control',
															'placeholder' => '',
															'required' => 'required'
														])); ?>

														
												</div>
											</div>
											<?php echo e(Form::bsText('nombres', '', [
												'class'       => 'form-control',
												'label'       => 'Nombres Completo / Razon Social:',
												'class_cont'  => 'col-md-8',
												'placeholder' => '',
												'required' => 'required'
											])); ?>


											</div>
										</div>
						

										</div>

										<div class="tab-pane" id="tab2">
											<?php echo $Personas_direccion->generate(); ?>

										</div>
										
										<div class="tab-pane" id="tab3">
											
											<?php echo e(Form::bsEmail('correo_pricipal', '', [
												'class'       => 'form-control',
												'label'       => 'Correo Principal',
												'class_cont'  => 'col-md-6',
												'placeholder' => '',
												'required' => 'required'
																								
											])); ?>


											<?php echo e(Form::bsEmail('correo_secundario', '', [
												'class'       => 'form-control',
												'label'       => 'Correo Secundario:',
												'class_cont'  => 'col-md-6',
												'placeholder' => '',
												'required' => 'required'
																								
											])); ?>

											<div class="col-md-12"></div>

											<?php echo e(Form::bsText('telefono_casa', '', [
												'class'       => 'form-control',	
												'label'       => 'Telefono Casa:',	
												'placeholder' => '',
												'class_cont'  => 'col-md-4',
																								
											])); ?>

											<?php echo e(Form::bsText('telefono_movil', '', [
												'class'       => 'form-control',
												'label'       => 'Telefono Movil:',
												'class_cont'  => 'col-md-4',
												'placeholder' => '',
																								
											])); ?>

											<?php echo e(Form::bsText('telefono_oficina', '', [
												'class'       => 'form-control',
												'label'       => 'Telefono Oficina:',
												'class_cont'  => 'col-md-4',
												'placeholder' => '',
																								
											])); ?>


											
										</div>

										<div class="tab-pane" id="tab4">

											<?php echo e(Form::bsPassword('password', '', [
												'class'       => 'form-control',
												'id' 		  => 'password',
												'label'       => 'Contraseña:',
												'class_cont'  => 'col-md-12',
												'placeholder' => '',
												'required' => 'required'
																								
											])); ?>


											<?php echo e(Form::bsPassword('rpassword2', '', [
												'class'       => 'form-control',
												'id'		  => 'rpassword',
												'label'       => 'Confirma Contraseña:',
												'class_cont'  => 'col-md-12',
												'placeholder' => '',
												'required' => 'required'
																								
											])); ?>

						
											
											<div class="col-md-12"></div>
											<?php echo e(Form::bsSelect('Pregunta_1', $preguntas, '', [
												'label' => 'pregunta Secreta',
												'class_cont'  => 'col-md-4',
												'required' => 'required'
											
											])); ?>

											
											<?php echo e(Form::bsText('Respuesta_1', '', [
												'class'       => 'form-control',
												'label'       => 'Repuesta:',
												'class_cont'  => 'col-md-8',
												'placeholder' => '',
												'required' => 'required'
																								
											])); ?>

												<div class="col-md-12"></div>
											<?php echo e(Form::bsSelect('Pregunta_2', $preguntas, '', [
												'label' => 'pregunta Secreta',
												'class_cont'  => 'col-md-4',
												'required' => 'required'
												
											])); ?>

											
											<?php echo e(Form::bsText('Respuesta_2', '', [
												'class'       => 'form-control',
												'label'       => 'Repuesta:',
												'class_cont'  => 'col-md-8',
												'placeholder' => '',
												'required' => 'required'
																								
											])); ?>

												<div class="col-md-12"></div>
											<?php echo e(Form::bsSelect('Pregunta_3', $preguntas, '', [
												'label' => 'pregunta Secreta',
												'class_cont'  => 'col-md-4',
												'required' => 'required'
												
											])); ?>

											
											<?php echo e(Form::bsText('Respuesta_3', '', [
												'class'       => 'form-control',
												'label'       => 'Repuesta:',
												'class_cont'  => 'col-md-8',
												'placeholder' => '',
												'required' => 'required'
																								
											])); ?>


										</div>
									</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<a href="javascript:;" class="btn default button-previous">
												<i class="fa fa-angle-left"></i> Atras </a>
											<a href="javascript:;" class="btn btn-outline green button-next"> Siguiente
												<i class="fa fa-angle-right"></i>
											</a>
											<button type="" class="btn green button-submit" id="guardar"> Guardar <i class="fa fa-check"></i></button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			
			<?php echo Form::close(); ?>

		
		</div>
		<script type="text/javascript" charset="utf-8" async defer>
		var ruta= "<?php echo e(asset('public/img/usuarios')); ?>";
		</script>
		<?php echo $__env->make('base::partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</body>
</html>