<?php $__env->startSection('content-top'); ?>
	<?php echo $__env->make('base::partials.botonera', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	
	<?php echo $__env->make('base::partials.ubicacion', ['ubicacion' => ['Administrador', 'Perfiles']], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<?php echo $__env->make('base::partials.modal-busqueda', [
		'titulo' => 'Buscar Usuarios.',
		'columnas' => [
			'Nombre' => '100'
		]
	], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<div class="row">
		<div class="col-sm-6 col-xs-12">
			<?php echo Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']); ?>

				<?php echo e(Form::hidden('permisos', '', ['id' => 'permisos'])); ?>


				<?php echo e(Form::bsText('nombre', '', [
					'label' 		=> 'Nombre',
					'placeholder' 	=> 'Nombre',
					'required' 		=> 'required',
					'class_cont' 	=> ''
				])); ?>

			<?php echo Form::close(); ?>

		</div>

		<div class="form-group col-sm-6 col-xs-12">
			<label for="arbol">Permisos de Usuario:</label>
			<input id="input_buscar" name="input_buscar" class="form-control" type="text" placeholder="Buscar" value="" /><br />

			<div id="arbol"></div>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('css'); ?>
<style type="text/css" media="screen">
	#arbol{
		min-height: 200px;
	}
</style>
<?php $__env->stopPush(); ?>
<?php echo $__env->make(isset($layouts) ? $layouts : 'base::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>