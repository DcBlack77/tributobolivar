 <?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-6">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="1349">1349</span></br>Formas
                </div>
                <div class="desc"> Total Forma 001 Aprobadas </div>
            </div>
        </a>

    </div>
    <div class="col-md-6">
        <a class="dashboard-stat dashboard-stat-v2 green" href="#">
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="12,5">12,5</span></br> Formas </div>
                    <div class="desc">  Total Forma 001 En Espera </div>
            </div>
        </a>
    </div>
    <div class="col-md-8" style="margin-left: 5%;">
        <div id="grafica"></div>

        <div class="jumbotron" style="background: #fff;" align="center">
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
        </div>

    </div>
</div>




</div>




<?php $__env->stopSection(); ?>
<?php echo $__env->make('base::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>