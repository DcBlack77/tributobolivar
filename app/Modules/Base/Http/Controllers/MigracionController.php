<?php

namespace App\Modules\Base\Http\Controllers;

//Dependencias
use DB;
use URL;
use Yajra\Datatables\Datatables;
use Session;
//Controlador Padre
use App\Modules\Base\Http\Controllers\Controller;
use Carbon\Carbon;
//Request
use App\Http\Requests\Request;
use App\Modules\Base\Http\Requests\UsuariosRequest;

//Modelos
use App\Modules\Base\Models\Usuario;
use Excel;

use App\Modules\Base\Models\TipoPersona;
use App\Modules\Base\Models\Personas;
use App\Modules\Base\Models\PersonasDetalles;
use App\Modules\Base\Models\PersonasBancos;
use App\Modules\Base\Models\PersonasCorreo;
use App\Modules\Base\Models\PersonasTelefono;
use App\Modules\Ventas\Models\Vendedores;
use App\Modules\Ventas\Models\VendedoresSucusal;
use App\Modules\Contratos\Models\Contratos;

class MigracionController extends Controller {
	protected $titulo = 'Migracion';

	public $js = ['Migracion'];
	public $css = ['Migracion'];

	public $librerias = [
		'ladda',
	];

	public function index() {	
		return $this->view('base::migracion');
	}
	public function contratos(Request $request) {	
		$mimes = [
			'text/csv',
			'application/vnd.ms-excel',
			'application/vnd.oasis.opendocument.spreadsheet',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		];

		$ruta = public_path('archivos/');
		$archivo = $request->file('subir');

		$mime = $archivo->getClientMimeType();

		if (!in_array($mime, $mimes)) {
			return "Error de Validación, la extension el archivo cargado debe ser: xls,xlsx,csv";
		}
		
		do {
			$nombre_archivo = str_random(20) . '.' . $archivo->getClientOriginalExtension();
		} while (is_file($ruta . $nombre_archivo));
			
		$archivo->move($ruta, $nombre_archivo);
		chmod($ruta . $nombre_archivo, 0777);

		$excel = Excel::selectSheetsByIndex(0)->load($ruta . $nombre_archivo)->get();
		//dd($excel);
		/* 
		frecuentas bd;
		1	m	05	Mensual los 05
		2	m	10	Mensual los 10
		3	m	15	Mensual los 15
		4	m	17	MENSUAL LOS 17
		5	m	20	Mensual los 20
		6	m	25	Mensual los 25
		7	m	30	Mensual los 30
		8	m	22	Mensual los 11 -> no va
		
		9	q	01,16	QUINCENAL 16 y 01
		10	q	15,30	DOCENTE 15 Y 30
		11	q	05,20	FERROMINERA 05 Y 20
		12	q	07,22	QUINCENALES 07 Y 22
		13	q	10,25	OBREROS DE EDUCACION 10 Y 25 
		
		*/

		
		$frecuencias =[
			'carga-sistema' 							=> 1,
			'mensual-los-15' 							=> 3,
			'quincenal-10-y-25' 						=> 13,
			'quincenal-15-y-30' 						=> 10,
			'mensual-los-05' 							=> 1,
			'mensual-los-10' 							=> 2,
			'mensual-los-20' 							=> 5,
			'quincenal-05-y-20' 						=> 11,
			'mensual-los-30' 							=> 7,
			'mensual-los-25' 							=> 6,
			'tdc-mensual-los-30' 						=> 7,
			'administrativo-gobernacion-mensual-los-30' => 7,
			'alcaldia-de-heres' 						=> 9,
			'administrativo-de-educacion' 				=> 13,
			'administrativo-gobernacion' 				=> 10,
			'consejo-legislativo' 						=> 10,
			'contraloria' 								=> 10,
			'cvg-mensual-los-05' 						=> 1,
			'cvg-mensual-los-30' 						=> 7,
			'cvg-quincenal' 							=> 10,
			'docente-15-y-30' 							=> 10,
			'docente-estadal' 							=> 13,
			'ferrominera-05-y-20' 						=> 11,
			'obreros-de-educacion-10-y-25' 				=> 13,
			'docente-ferrominera-05-y-20' 				=> 11,
			'docentes-mensual-los-30' 					=> 7,
			'ferrominera-mensual-los-05' 				=> 1,
			'fundacion-del-nino-quincenal' 				=> 10,
			'inviobras-quincenal' 						=> 10,
			'jubilado-de-educacion-mensual-los-30' 		=> 7,
			'policia-del-estado-bolivar' 				=> 10,
			'salud-publica-quincenal' 					=> 10,
			'jubilado-de-la-policia-mensual-los-30' 	=> 7,
			'obreros-15-y-30' 							=> 10,
			'tribunales-quincenal' 						=> 10,
			'policia-mensuales-los-15' 					=> 3,
			'policia-municipal-de-heres' 				=> 10,
			'salud-publica-mensual-los-30' 				=> 7,
			'quincenal-16-y-01' 						=> 9,
			'mensual-los-17' 							=> 4,
			'salud-publica-mensual-los-15' 			    => 3,
			'jubilado-salud-publica-mensual-los-30' 	=> 7,
			'quincenales-07-y-22' 						=> 12,
			'docente-estadal-mensual-los-25' 			=> 6,
		];


		$bancos = [ 
			'banco-del-sur-online' => 9,
			'banco-corpbanca' => 13,
			'banco-banesco' => 8,
			'banco-banesco-tdc' => 8,
			'banco-venezuela' => 2,
			'banco-del-sur-online-giros' => 9,
			'banco-corpbanca-giros' => 13,
			'banco-banesco-giros' => 8,
			'banco-venezuela-giros' => 2,
			'banco-mercantil' => 3,
			'banco-bicentenario' => 10,
			'banco-bicentenario-giros' => 10,
			'banco-mercantil-giros' => 3,
			'banco-provincial' => 4,
			'banco-provincial-giros' => 4,
			'cacrete' => 1,
			'cacrete-giros' => 1,
			'banco-banesco-tdc-giros' => 8,
			'banco-caroni' => 7,
			'banco-caroni-giros' => 7,
			'banco-venezuela-servicios' => 2,
			'banco-venezuela-servicios-giros' => 2,
			'plan-particular' => '',
			'banco-exterior' => 6,
			'banco-exterior-giros' => 6,
			'banco-del-sur' => 9,
			'banco-del-sur-giros' => 9,
			'banco-guayana' => 9,
			'banco-guayana-giros' => 9,
			'banco-caribe' => 5,
			'banco-caribe-giros' => 5,
			'banco-nacional-de-credito' => 11,
			'banco-nacional-de-credito-giros' => 11,
			'banco-occidental-de-descuento' => 12,
			'banco-bod' => 12,
			'centro-clinico-provincial' => 4,
			'centro-clinico-bicentenario' => 10,
			'centro-clinico-mercantil' => 3,
			'centro-clinico-banesco' => 8,
			'centro-clinico-banesco-tdc' => 8,
			'centro-clinico-venezuela' => 2,
			'centro-clinico-cacrete' => 1,
			'centro-clinico-bod' => 12,
			'centro-clinico-del-sur' => 9,
			'centro-clinico-caroni' => 7,
			'centro-clinico-banco-nacional-de-credito' => 11,
			'convenio' => '',
			'centro-clinico-bancaribe' => 5,
			'centro-clinico-exterior' => 6,
			'centro-clinico-plan-particular' => '',

		];
		$bancos_giros = [ 
			'banco-del-sur-online-giros' => 1,
			'banco-corpbanca-giros' => 1,
			'banco-banesco-giros' => 1,
			'banco-venezuela-giros' => 1,
			'banco-bicentenario-giros' => 1,
			'banco-mercantil-giros' => 1,
			'banco-provincial-giros' => 1,
			'cacrete-giros' => 1,
			'banco-banesco-tdc-giros' => 1,
			'banco-caroni-giros' => 1,
			'banco-venezuela-servicios-giros' => 1,
			'banco-exterior-giros' => 1,
			'banco-del-sur-giros' => 1,
			'banco-guayana-giros' => 1,
			'banco-caribe-giros' => 1,
			'banco-nacional-de-credito-giros' => 1
		];

		
		try {
			$num =0;
			foreach ($excel as $key => $value){
				//vamos ver si el tipo de persona existe
				$contrato =[];
			    $tipo_persona = TipoPersona::updateOrCreate(
					['nombre' => strtoupper($value->nacionalidad)]
				);
				//persona
				$persona = Personas::updateOrCreate(
					['dni' => strtoupper($value->cedula)],
					[
						'nombres' 		  => $value->titular,
						'tipo_persona_id' => $tipo_persona->id
					]
				);
				//persona detalle
				if($tipo_persona->id == 1 || $tipo_persona->id == 2 ){
					$PersonasDetalles = PersonasDetalles::updateOrCreate(
						['personas_id' => $persona->id ],
						[
							"profesion_id"  	=> null,
							"sexo" 				=> null,
						]
					);	
				}
				
				switch ($value->tipocta) {
					case 'AHO':
						$tipo_cuenta = 1;
						break;
					case 'CCO':
						$tipo_cuenta = 2;
						break;
					case '***':
						$tipo_cuenta = 3;
						break;
					
					default:
						# code...
						break;
				}
				//$bancos
				//NroCta
				$_banco = intval($bancos[str_slug($value->banco)]);
				$p_banco = null;
				if($_banco != 0){
					$banco_persona = PersonasBancos::where('bancos_id',intval($_banco) ) 
					->where('cuenta',$value->nrocta)
					->where('personas_id', $persona->id)
					->first();
	
					if(count($banco_persona) == 0){	
						
						if($value->nrocta = '' ){
							$n_cuenta = 00000;
						}else{
							$n_cuenta = $value->nrocta;
						}
						$banco_persona = PersonasBancos::create([	
						'bancos_id'       => intval($_banco),  
						'personas_id'     => $persona->id,  
						'cuenta'  		  => $n_cuenta, 
						'tipo_cuenta_id'  => $tipo_cuenta 
						]); 
	
						$p_banco = $banco_persona->id;
					} else{
						$p_banco =$banco_persona->id;
					}
				}
					
				if($value->cedula_del_asesor == 0 ){
					$persona_vendedor = 2;
				
				}else{	

					$persona_vendedor =  Personas::where('dni',$value->cedula_del_asesor)->first();
					if(count($persona_vendedor) == 0){
						$persona_vendedor = 2;
					}else{
						$persona_vendedor = $persona_vendedor->id;
					}
					//$vendedor = Vendedores::where('personas_id', $persona_vendedor)->first();

				}
				$vendedor = Vendedores::where('personas_id', $persona_vendedor)->first();

				if(count($vendedor) == 0){
					$persona_vendedor = 2;
					$vendedor = Vendedores::where('personas_id', $persona_vendedor)->first();
				}
	
				switch ($value->bookiesucursal) {
					case 'CBO':
						$sucural = 1;
						break;
					case 'PLC':
						$sucural = 2;
						break;
					case 'POZ':
						$sucural = 3;
						break;
					case 'TGR':
						$sucural = 4;
						break;
					
					default:
						$sucural = 1;
						break;
				}

				//1/1/1900
				if($value->fecha1cobro == '1999/1/1'){
					$fecha = null;
				}else{
					$fecha = $value->fecha1cobro;
				}
				//calcular fecha de vencimiento
				$vencimiento = "";

				if($value->totalcuotas > 6){
					$vencimiento =  $value->fechacarga->addYear(4);
				}else if($value->totalcuotas <= 6){
					$vencimiento = $value->fechacarga->addYear();
				}
				

				//frecuencia de pago --10
				$fre = 10;
				if(array_key_exists(str_slug($value->frecuencia),$frecuencias)){
					$fre = $frecuencias[str_slug($value->frecuencia)];
				}
				//estado contrato 
				$estad = true;
				$contra_estatus = 6;
				if(str_slug($value->estado) === 'activo'){
					$estad = false;
					$contra_estatus = 1;
				}

				//contrato cobrado?
				$cobrado = false;
				if($value->saldopendiente == 0 ){
					$cobrado == true;
				}

				//saber si un contrato es giro

				$giros_pagados = null;
				$giros 		   = null;
				$giros_valor   = null;
				
				if(array_key_exists(str_slug($value->banco),$bancos_giros)){
					$giros_pagados = $value->totalcuotas - $value->CuotasPendientes;
					$giros = $value->totalcuotas;
					$giros_valor = $value->montocuotas;
				}
				
				/* --------------contratos--------- */
				$contrato = [
					"id" 					    => $value->contratoid,
					"planilla" 					=> $value->nrocontrato,
					"titular"  					=> $persona->id,
					"meses" 					=> null,
					"observaciones" 			=> null,
					"tipo_contrato" 			=> 0,
					"vendedor_id" 				=> $vendedor->id,
					"sucursal_id" 				=> $sucural,
					"empresa_id"  				=> Session::get('empresa'),
					"cargado"     				=> $value->fechacarga,
					"primer_cobro" 				=> $fecha,
					"vencimiento"   			=> $vencimiento,
					"inicio" 					=> $value->fechacontrato,
					"plan_detalles_id" 			=> null,
					"frecuencia_pagos_id"       => $fre ,
					"tipo_pago"					=> 2,
					"total_contrato"            => $value->montocontrato,
					"total_pagado"				=> $value->saldopendiente,		
					"cuota_valor"				=> $value->montocuotas,
					"cuotas"					=> $value->totalcuotas,
					"cuotas_pagadas"			=> $value->totalcuotas - $value->CuotasPendientes,
					"giros_pagados"				=> $giros_pagados,
					"giros"						=> $giros,
					"giro_valor"				=> $giros_valor,
					"personas_bancos_id"        => $p_banco,
					"beneficiarios"             => 0,
					"inicial" 					=> $value->montoinicial,
					"fecha_incial" 				=> $value->fecha1cobroinicial,
					"fecha_pago"        		=> null,
					"anulado" 					=> $estad,
					"bookie" 					=> true,
					"cobrando"					=> $cobrado,
					"renovaciones"				=> 0,
					"estatus_contrato_id"		=> $contra_estatus
				];
				Contratos::create($contrato);
				$num++;
			}
		} catch (Exception $e) { }
		
		return ['s' => 's', 'msj' => 'Carga Completa', 'cantidades' => $num ];
	}

	public function vendedores(Request $request) {	
		$mimes = [
			'text/csv',
			'application/vnd.ms-excel',
			'application/vnd.oasis.opendocument.spreadsheet',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		];

		$ruta = public_path('archivos/');
		$archivo = $request->file('subir');

		$mime = $archivo->getClientMimeType();

		if (!in_array($mime, $mimes)) {
			return "Error de Validación, la extension el archivo cargado debe ser: xls,xlsx,csv";
		}
		
		do {
			$nombre_archivo = str_random(20) . '.' . $archivo->getClientOriginalExtension();
		} while (is_file($ruta . $nombre_archivo));
			
		$archivo->move($ruta, $nombre_archivo);
		chmod($ruta . $nombre_archivo, 0777);

		$excel = Excel::selectSheetsByIndex(0)->load($ruta . $nombre_archivo)->get();
		//dd($excel);
	

	
		try {
			$num =0;
			foreach ($excel as $key => $value){
				//vamos ver si el tipo de persona existe
				
			/* 	CÓDIGO	
				NOMBRES	
				CÉDULA	
				STATUS
				FECHA RETIRO		
				Correo Electonico
			*/	
				
			    $tipo_persona = 1;
				//persona

				if($value->cedula == " "){
					continue;
				}

				$persona = Personas::updateOrCreate(
					['dni' => strtoupper($value->cedula)],
					[
						'nombres' 		  => $value->nombres,
						'tipo_persona_id' => $tipo_persona
					]
				);

				PersonasCorreo::updateOrCreate(
					['personas_id' => $persona->id],
					[
						'principal' 	  => 1,
						'correo' => $value->correo
					]
				);
				PersonasTelefono::updateOrCreate(
					['personas_id' => $persona->id],
					[
						'tipo_telefono_id' 	=> 1,
						'principal' 	    => 1,
						'numero' 			=> $value->felefono
					]
				);


				$Vendedores = Vendedores::updateOrCreate(
					['personas_id' => $persona->id ],
					[
						'estatus' => 1,
						'codigo'  => $value->codigo
					]
				);

				$sucursales=[
					1,
					2,
					3,
					4
				];
				foreach ($sucursales as $value) {
					VendedoresSucusal::create([
						'vendedor_id' => $Vendedores->id,
						'sucursal_id' => $value,
						'empresa_id' => Session::get('empresa')
					]);
				} 

				$num++;
		
			}
		} catch (Exception $e) { }
		
		return ['s' => 's', 'msj' => 'Carga Completa', 'cantidades' => $num ];
	}
}