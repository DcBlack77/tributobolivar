<?php 


namespace App\Modules\Base\Http\Requests;

use Illuminate\Foundation\Http\Request;

class RegistroRequest extends Request {
   //public function rules() {
    //    return [
        protected $reglasArr = [
            'tipo_persona'     =>  ['required'],
           'dni'               =>  ['required'],
           'nombres'           =>  ['required', 'nombre', 'min:3', 'max:50'],
           'estados_id'        =>  ['required', 'nombre', 'min:3', 'max:50'],
           'ciudades_id'       =>  ['required', 'nombre', 'min:3', 'max:50'],
           'municipios_id'     =>  ['required', 'nombre', 'min:3', 'max:50'],
           'parroquias_id'     =>  ['required', 'nombre', 'min:3', 'max:50'],
           'direccion'         =>  ['required', 'nombre', 'min:3', 'max:50'],
           'correo_pricipal'   =>  ['required','max:50', 'unique:personas_correo,correo'],
           'correo_secundario' =>  ['required','max:50', 'unique:personas_correo,correo'],
           'telefono_casa'     =>  ['required'],
           'telefono_movil'    =>  ['required'],
           'telefono_oficina'  =>  ['required'],
           'password'          =>  ['required', 'password', 'min:8', 'max:50'],
           'rpassword'         =>  ['required', 'password', 'min:8', 'max:50'],
           'Pregunta_1'        =>  ['required'],
           'Respuesta_1'       =>  ['required'],
           'Pregunta_2'        =>  ['required'],
           'Respuesta_2'       =>  ['required'],
           'Pregunta_3'        =>  ['required'],
           'Respuesta_3'       =>  ['required'],
                     
        ];
 //  }

  /* public function recordar() {
       return $this->get('recordar', 'false') === 'true';
   }

   public function credenciales() {
       return $this->only('usuario', 'password');
   }

   public function authorize() {
       return true;
   }
   */
}
/// validacion de mariverga
