<?php

namespace App\Modules\Base\Http\Requests;

use App\Http\Requests\Request;

class PreguntaSeguridadRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:100'], 
		//'slug' => ['required', 'min:3', 'max:100', 'unique:pregunta_seguridad,slug']
	];
	   
}