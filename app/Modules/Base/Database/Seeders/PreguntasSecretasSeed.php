<?php

namespace App\Modules\Base\Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

use App\Modules\Base\Models\PreguntaSeguridad;

class PreguntasSecretasSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
         $data = [
             'Apellido de soltera de la Madre',
             'Ciudad en la que nacio',
             'Nombre de su mascota',
             'Equipo favorito',
             'Marca de su primero vehiculo',
             'Lugar nacimiento del padre',
             'Apellido de soltera de su abuela',
             'Amparito pues'
         ];

         DB::beginTransaction();
         try{
             foreach ($data as $datas) {
                 PreguntaSeguridad::create([
                     'nombre' => $datas,
                     'slug'   => str_slug($datas)
                 ]);
             }
         }catch(Exception $e){
             DB::rollback();
             echo "Error ";
         }
         DB::commit();
     }
}
