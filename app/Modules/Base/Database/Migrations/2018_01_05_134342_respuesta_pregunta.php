<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RespuestaPregunta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuesta_pregunta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usuario_id')->unsigned();
            $table->integer('pregunta_seguridad_id')->unsigned();
            $table->text('respuesta');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('usuario_id')
                  ->references('id')->on('app_usuario')
                  ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('pregunta_seguridad_id')
                  ->references('id')->on('pregunta_seguridad')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

  
    public function down()
    {
       Schema::dropIfExists('respuesta_pregunta');
    }
}
