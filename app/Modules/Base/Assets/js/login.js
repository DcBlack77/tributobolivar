$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
    },
    complete: function(x, e, o) {
        $("#cargando").animate({ opacity: 0 }, {
            queue: false,
            complete: function() {
                $(this).css({ display: 'none' });
            }
        });
    },
    error: function(r) {
        var res = r.responseJSON,
            html = "";

        for (var i in res) {
            html += res[i].join("<br />") + "<br />";
        }

        new PNotify({
            title: 'Error de validacion',
            text: html,
            type: 'error',
            hide: true
        });
    },
    timeout: 0,
    cache: false
});
aplicacion = new app('submit_form');

var app = {
    form: '',
    nombre: '',
    password: '',

    init: function() {
        this.form = $("#formulario");
        this.nombre = $("input[name='nombre']", this.form).val('').focus();
        this.password = $("input[name='password']", this.form).val('');
        this.empresa = $("select[name='empresa']", this.form).val('');
        this.recordar = $("input[name='recordar']", this.form);

        $("button", this.form).click(this.validarAuth);

        this.form.submit(function() {
            return false;
        });

        $(".form-control", this.form).keypress(this.validarAuth);
    },

    validarAuth: function(e) {
        if (e.type == 'click' || e.which == 13) {
            app._validarAuth();
        }
    },

    _validarAuth: function() {
        if (this.nombre.val() === '') {
            this.nombre.focus();
        } else if (this.password.val() === '') {
            this.password.focus();
        } else {
            this.buscar();

        }
    },

    buscar: function() {
        $("#boton").prop("disable", true);

        $.ajax($url + 'validar', {
            type: "POST",
            data: {
                usuario: this.nombre.val(),
                password: this.password.val(),
                recordar: this.recordar.prop("checked"),
                empresa: this.empresa.val()
            },

            success: function(r) {
                if (r.s === "n") {
                    $("#boton").prop("disable", false);

                    new PNotify({
                        title: 'No se puedo Autenticar',
                        text: r.msj,
                        type: 'error',
                        hide: true
                    });

                    return false;
                }

                if (r.s === "s") {
                    //location.reload();
                    location.href = $url.replace(/\/+$/, '');
                }

                return false;
            }
        });
    },


};

var selectFormGroup = function(event) {
    event.preventDefault();

    var $selectGroup = $(this).closest('.input-group-select'),
        param = $(this).attr("href").replace("#", ""),
        concept = $(this).text();

    $selectGroup.find('.concept').text(concept);
    $selectGroup.find('.input-group-select-val').val(param);
}

var countFormGroup = function($form) {
    return $form.find('.form-group').length;
};

$('.cont-persona').on('click', '.dropdown-menu a', selectFormGroup);


$('.user').blur(function() {
    if ($(this).val() == '') {
        $('#foto').prop('src', ruta + '/user.png');
        return false;
    }
    validar($(this).val(), '.user');
});

$('#register-btn').on('click', function() {
    $('#login').css('display', 'none');
    $('#registro').css('display', 'block');
});
$('#forget-password').on('click', function() {
    $('#login').css('display', 'none');
    $('#registro').css('display', 'none');
    $('#recuperar').css('display', 'block');
});
$('.cerrar').on('click', function() {
    $('#login').css('display', 'block');
    $('#registro').css('display', 'none');
    $('#recuperar').css('display', 'none');
});




/* login selector  */

// $("#btcselect").click(function(){
//     console.log("Hello world!");
// });

$("#dni").numeric({ min: 0 });


$("#btn-1").click(function() {
    console.log("validar v");
    $("#dni").numeric({ min: 0 });
    // $("#dni").mask("99999999");
});

$("#btn-2").click(function() {
    console.log("validar E");
});

$("#btn-3").click(function() {
    console.log("validar G");
    $("#dni").numeric({ min: 0 });

    $("#dni").mask("99999999-9");
});

$("#btn-4").click(function() {
    console.log("validar J");
    $("#dni").numeric({ min: 0 });

    $("#dni").mask("99999999-9");
});


$("#correo_secundario").blur(function() {
    if ($("#correo_pricipal").val() == $("#correo_secundario").val()) {
        alert('El correo Secundario tiene que ser diferente al principal');
        return false;
    }
});


/* fin login selector  */

$('#guardar').on('click', function() {

    if ($("#correo_pricipal").val() == $("#correo_secundario").val()) {
        alert('El correo Secundario tiene que ser diferente al principal');
        return false;
    }

    $('#submit_form').ajaxSubmit({
        'url': $url + 'registro',
        'type': 'POST',
        'success': function (r) {
            if (r.s == 's') {
                if ($('#password').val() != $('#rpassword').val()) {
                    new PNotify({
                        title: 'Error de validacion',
                        text: 'Las dos claves son distintas!!',
                        type: 'error',
                        hide: true
                    });
                    return false;
                }
                location.reload();
            }
        }
    });
});


/* $('#formulario3').ajaxSubmit({
    'url': $url + 'registro',
    'type': 'POST',
    'success': function(r) {
        console.log('listo');

        if (r.s == 's') {
            new PNotify({
                title: 'success',
                text: r.msj,
                type: 'success',
                hide: true
            });

            location.reload();
        }
    }
}); */
$('#estados_id').change(function() {
    aplicacion.selectCascada($(this).val(), 'ciudades_id', 'ciudades');
    aplicacion.selectCascada($(this).val(), 'municipio_id', 'municipios');
});

$('#municipios_id').change(function() {
    aplicacion.selectCascada($(this).val(), 'parroquia_id', 'parroquias');
});

$('#parroquia_id').change(function() {
    aplicacion.selectCascada($(this).val(), 'sector_id', 'sectores');
});


app.init();

//valida DNI, RIF y CORREO
function validar($dato, $id) {
    /*
        $dato = dato a validar
        $tipo = que campo validar
    */
    $.ajax({
        url: $url + 'foto',
        type: 'POST',
        data: {
            'usuario': $dato
        },
        success: function(r) {
            console.log();
            if (r.foto != '') {
                $('#foto').prop('src', ruta + '/' + r.foto);
            } else {
                $('#foto').prop('src', ruta + '/user.png');
            }
        }
    });
}

//RECUPERACIÖN DE PASS
$('#recuperar-submit-btn').on('click', function recuperar(){
    $.ajax({
        url: $url + 'recuperar/enviar',
        type: 'POST',
        data: $('#formulario3').serialize(),
        success: function() {
            console.log('envio el correo');
        }
    });
});
