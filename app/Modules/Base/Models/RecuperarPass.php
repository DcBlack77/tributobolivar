<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;

class RecuperarPass extends Model
{
    protected $table = 'password_resets';
    protected $fillable = ["email","token"];

}
