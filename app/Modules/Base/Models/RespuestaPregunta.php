<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;



class RespuestaPregunta extends Model
{
    protected $table = 'respuesta_pregunta';
    protected $fillable = ["usuario_id","pregunta_seguridad_id","respuesta"];
    
    
}