<?php

namespace App\Modules\Base\Models;

use App\Modules\Base\Models\Modelo;

class BancoTipoCuenta extends modelo
{
    protected $table = 'banco_tipo_cuenta';
    protected $fillable = ["nombre"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Banco Tipo Cuenta'
        ]
    ];
}