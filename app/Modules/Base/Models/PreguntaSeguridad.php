<?php

namespace App\Modules\Base\Models;

use App\Modules\Base\Models\Modelo;



class PreguntaSeguridad extends Modelo
{
    protected $table = 'pregunta_seguridad';
    protected $fillable = ["nombre","slug"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Pregunta Seguridad'
    ],
    /* 'slug' => [
        'type' => 'text',
        'label' => 'Slug',
        'placeholder' => 'Slug del Pregunta Seguridad'
    ] */
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}