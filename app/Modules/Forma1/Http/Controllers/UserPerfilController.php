<?php

namespace App\Modules\Forma1\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Modules\Forma1\Http\Controllers\Controller;

//modelo 
use App\Modules\Base\Models\Personas;

class UserPerfilController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return $this->view('forma1::perfiluser', [
			'Personas' => new Personas()
            //'Personas_detalles' => new PersonasDetalles(),
            //'Personas_direccion' => new PersonasDireccion()
		]);
        //return  $this->view('forma1::perfiluser');

    }

    /* user perfiles usuario */
    protected function personas($request, $id){
        //procesa los datos antes de guardar
            
            $persona = [
                "tipo_persona_id" => $request->tipo_persona_id,
                "dni"             => $request->dni,
                "nombres"         => $request->nombres
            ];
            return $persona;
        }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('forma1::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('forma1::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('forma1::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
