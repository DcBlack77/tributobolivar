<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Impuestos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidad_tributaria', function (Blueprint $table) {
            $table->increments('id');
            $table->string('periodo')->unique();
            $table->integer('costo');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('articulos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->text('descripcion');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('impuestos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('impuesto');
            $table->integer('articulo_id')->unsigned()->nulleable();
            $table->string('codigo');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('articulo_id')
                ->references('id')->on('articulos')
                ->onDelete('cascade')->onUpdate('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('impuestos');
        Schema::dropIfExists('articulos');
        Schema::dropIfExists('unidad_tributaria');
    }
}
