<?php

namespace App\Modules\Impuestos\Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

use App\Modules\Impuestos\Models\UnidadTributaria;

class UTSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['2010', 65],
            ['2011', 76],
            ['2012', 90],
            ['2013', 107],
            ['2014', 127],
            ['2015', 150],
            ['2016', 177],
            ['2017', 300],

        ];

        DB::beginTransaction();
        try{
            foreach ($data as $ut) {
                UnidadTributaria::create([
                    'periodo'   => $ut[0],
                    'costo'     => $ut[1]
                ]);
            }
        }catch(Exception $e){
            DB::rollback();
            echo "Error ";
        }
        DB::commit();
    }
}
