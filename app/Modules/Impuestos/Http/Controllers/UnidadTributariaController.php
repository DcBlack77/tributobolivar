<?php

namespace App\Modules\Impuestos\Http\Controllers;

//Controlador Padre
use App\Modules\Impuestos\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Impuestos\Http\Requests\UnidadTributariaRequest;

//Modelos
use App\Modules\Impuestos\Models\UnidadTributaria;

class UnidadTributariaController extends Controller
{
    protected $titulo = 'Unidad Tributaria';

    public $js = [
        'UnidadTributaria'
    ];

    public $css = [
        'UnidadTributaria'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('impuestos::UnidadTributaria', [
            'UnidadTributaria' => new UnidadTributaria()
        ]);
    }

    public function nuevo()
    {
        $UnidadTributaria = new UnidadTributaria();
        return $this->view('impuestos::UnidadTributaria', [
            'layouts' => 'base::layouts.popup',
            'UnidadTributaria' => $UnidadTributaria
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $UnidadTributaria = UnidadTributaria::find($id);
        return $this->view('impuestos::UnidadTributaria', [
            'layouts' => 'base::layouts.popup',
            'UnidadTributaria' => $UnidadTributaria
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $UnidadTributaria = UnidadTributaria::withTrashed()->find($id);
        } else {
            $UnidadTributaria = UnidadTributaria::find($id);
        }

        if ($UnidadTributaria) {
            return array_merge($UnidadTributaria->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(UnidadTributariaRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $UnidadTributaria = $id == 0 ? new UnidadTributaria() : UnidadTributaria::find($id);

            $UnidadTributaria->fill($request->all());
            $UnidadTributaria->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $UnidadTributaria->id,
            'texto' => $UnidadTributaria->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            UnidadTributaria::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            UnidadTributaria::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            UnidadTributaria::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = UnidadTributaria::select([
            'id', 'periodo', 'costo', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}
