<?php

namespace App\Modules\Impuestos\Http\Controllers;

//Controlador Padre
use App\Modules\Impuestos\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Impuestos\Http\Requests\ImpuestosRequest;

//Modelos
use App\Modules\Impuestos\Models\Impuestos;

class ImpuestosController extends Controller
{
    protected $titulo = 'Impuestos';

    public $js = [
        'Impuestos'
    ];

    public $css = [
        'Impuestos'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('impuestos::Impuestos', [
            'Impuestos' => new Impuestos()
        ]);
    }

    public function nuevo()
    {
        $Impuestos = new Impuestos();
        return $this->view('impuestos::Impuestos', [
            'layouts' => 'base::layouts.popup',
            'Impuestos' => $Impuestos
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Impuestos = Impuestos::find($id);
        return $this->view('impuestos::Impuestos', [
            'layouts' => 'base::layouts.popup',
            'Impuestos' => $Impuestos
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Impuestos = Impuestos::withTrashed()->find($id);
        } else {
            $Impuestos = Impuestos::find($id);
        }

        if ($Impuestos) {
            return array_merge($Impuestos->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(ImpuestosRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Impuestos = $id == 0 ? new Impuestos() : Impuestos::find($id);

            $Impuestos->fill($request->all());
            $Impuestos->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Impuestos->id,
            'texto' => $Impuestos->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Impuestos::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Impuestos::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Impuestos::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Impuestos::select([
            'impuestos.id',
            'impuestos.impuesto',
            'articulos.codigo as articulo',
            'impuestos.codigo',
            'impuestos.deleted_at'
        ])->join('articulos','articulos.id','=','impuestos.articulo_id');

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}
