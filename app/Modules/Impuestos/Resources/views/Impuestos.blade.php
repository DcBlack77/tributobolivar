@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')

    @include('base::partials.ubicacion', ['ubicacion' => ['Impuestos']])

    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Impuestos.',
        'columnas' => [
            'Impuesto' => '75',
    		'Articulo' => '10',
    		'Codigo' => '15'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Impuestos->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection
