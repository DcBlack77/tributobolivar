@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')

    @include('base::partials.ubicacion', ['ubicacion' => ['Articulos']])

    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Articulos.',
        'columnas' => [
            'Codigo' => '15',
		    'Descripcion' => '85'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Articulos->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection
