var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.ajax.reload();
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [
			{"data":"impuesto","name":"impuesto"},
			{"data":"articulo","name":"articulo"},
			{"data":"codigo","name":"codigo"}
		]
	});

	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});
});
