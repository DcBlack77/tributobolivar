<?php

Route::group(['middleware' => 'web', 'prefix' => 'empresa', 'namespace' => 'App\\Modules\Empresa\Http\Controllers'], function()
{

        Route::group(['prefix' => 'empresa'], function() {
            Route::get('/',                 'EmpresaController@index');
            Route::get('nuevo',             'EmpresaController@nuevo');
            Route::get('cambiar/{id}',      'EmpresaController@cambiar');
            
            Route::get('buscar/{id}',       'EmpresaController@buscar');

            Route::post('guardar',          'EmpresaController@guardar');
            Route::put('guardar/{id}',      'EmpresaController@guardar');

            Route::delete('eliminar/{id}',  'EmpresaController@eliminar');
            Route::post('restaurar/{id}',   'EmpresaController@restaurar');
            Route::delete('destruir/{id}',  'EmpresaController@destruir');

            Route::get('datatable',         'EmpresaController@datatable');
        });
     
        Route::group(['prefix' => 'sucursal'], function() {
			Route::get('/', 				'SucursalController@index');
			Route::get('nuevo', 			'SucursalController@nuevo');
			Route::get('cambiar/{id}', 		'SucursalController@cambiar');
			
			Route::get('buscar/{id}', 		'SucursalController@buscar');
			Route::get('ciudades/{id}',     'SucursalController@ciudades');
			Route::get('municipios/{id}',   'SucursalController@municipios');
			Route::get('parroquias/{id}',   'SucursalController@parroquias');
			Route::get('sectores/{id}',    	'SucursalController@sectores');
			Route::post('guardar',			'SucursalController@guardar');
			Route::put('guardar/{id}', 		'SucursalController@guardar');

			Route::delete('eliminar/{id}', 	'SucursalController@eliminar');
			Route::post('restaurar/{id}', 	'SucursalController@restaurar');
			Route::delete('destruir/{id}', 	'SucursalController@destruir');

			Route::get('datatable', 		'SucursalController@datatable');
		}); 
        
        Route::group(['prefix' => 'bancos_empresa'], function() {
            Route::get('/', 				'BancosEmpresaController@index');
            Route::get('nuevo', 			'BancosEmpresaController@nuevo');
            Route::get('cambiar/{id}', 		'BancosEmpresaController@cambiar');
            
            Route::get('buscar/{id}', 		'BancosEmpresaController@buscar');

            Route::post('guardar',			'BancosEmpresaController@guardar');
            Route::put('guardar/{id}', 		'BancosEmpresaController@guardar');

            Route::delete('eliminar/{id}', 	'BancosEmpresaController@eliminar');
            Route::post('restaurar/{id}', 	'BancosEmpresaController@restaurar');
            Route::delete('destruir/{id}', 	'BancosEmpresaController@destruir');

            Route::get('datatable', 		'BancosEmpresaController@datatable');
        });

    //{{route}}
});