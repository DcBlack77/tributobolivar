<?php

namespace App\Modules\Pagos\Models;

use App\Modules\Base\Models\Modelo;



class Referencias extends Modelo
{
    protected $table = 'referencias';
    protected $fillable = ["monto","numero","banco"];
    protected $campos = [
    'monto' => [
        'type' => 'text',
        'label' => 'Monto',
        'placeholder' => 'Monto del Referencias'
    ],
    'numero' => [
        'type' => 'text',
        'label' => 'Numero',
        'placeholder' => 'Numero del Referencias'
    ],
    'banco' => [
        'type' => 'text',
        'label' => 'Banco',
        'placeholder' => 'Banco del Referencias'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}