<?php

namespace App\Modules\Pagos\Models;

use App\Modules\Base\Models\Modelo;
use App\Modules\Base\Models\Usuario;
use App\Modules\Impuestos\Models\Impuestos;



class Pagos extends Modelo
{
    protected $table = 'pagos';
    protected $fillable = ["impuesto_id","app_usuario_id","codigo","referencia","monto"];
    protected $campos = [
    'impuesto_id' => [
        'type' => 'select',
        'label' => 'Impuesto',
        'placeholder' => '- Seleccione Impuesto a Pagar',
        'url' => 'Agrega una URL Aqui!'
    ],
    'app_usuario_id' => [
        'type' => 'hidden',
        'label' => 'Usuario',
        'placeholder' => '- Seleccione un Usuario',
        'url' => 'Agrega una URL Aqui!'
    ],
    'codigo' => [
        'type' => 'hidden',
        'label' => 'Codigo',
        'placeholder' => 'Codigo del Pago'
    ],
    'referencia' => [
        'type' => 'text',
        'label' => 'Referencia',
        'placeholder' => 'Referencia del Pagos'
    ],
    'monto' => [
        'type' => 'number',
        'label' => 'Total a Pagar',
        'placeholder' => 'Referencia del Pagos'
    ],
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['impuesto_id']['options'] = Impuestos::pluck('impuesto', 'id');

    }

    public function usuario(){
        return $this->belongsToMany('App\Modules\Base\Models\Usuario', 'app_usuario_id');
    }

    public function impuestos(){
        return $this->belongsTo('App\Modules\Impuestos\Models\Impuestos', 'impuesto_id');
    }


}
