@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Referencias']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Referencias.',
        'columnas' => [
            'Monto' => '33.333333333333',
		'Numero' => '33.333333333333',
		'Banco' => '33.333333333333'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Referencias->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection