<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Modules\Contratos\Models\Contratos;
use Carbon\Carbon;
use DB;

class contratosvencidos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vencidos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'comando para poner los contratos al estatus vencidos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       //codigo a ejecutar 


         DB::beginTransaction();
        try{
           
             $date =  Carbon::now();
            Contratos::where('vencimiento', '<=',$date)
            ->update('estatus_contrato_id', 5);
           
 
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            //return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
           // return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();
    }
}
